new Vue({
    el: "#app",
    data:function(){
        return {
            status:'',
            Champions:'',
            idChampion: ''
        }
    },
    created: function () {
        this.loadQuote()
    },
    methods:{
        selectChamp: function (champ) {
            console.log(champ);
        },
        loadQuote: function () {
            this.status = "loading..";
            var Champions = this;
            axios.get('/php/getChampions.php')
                .then(function (response) {
                    console.log(response.data);
                    Champions.Champions = response.data;
                    Champions.status = true;
                })
                .catch(function (error) {
                    Champions.status = "An error occurred." + error;
                });
        }
    }
});

/* Event */

/* Components */

// Listes des champions
Vue.component('champion-item', {
    props: ['Champions'],
    template: '<div class="unicChampion" v-on:click="selectChamp(Champions.id)"><img :src="Champions.images" :alt="Champions.name" :title="Champions.name" :class="Champions.name" :data-id="Champions.id"/><br /><div class="nameChampion">{{ Champions.name }}</div></div>'
});