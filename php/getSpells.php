<?php
header('Content-Type: application/json');
$bdd = new PDO('mysql:host=localhost;dbname=applilol;charset=utf8', 'root', '');

$id_champions = intval($_GET['id_champions']);
$result = $bdd->prepare("SELECT s.name as name, S1, S2, S3, S4, S5, S6, spellButton, c.name as nameChampion, s.images as spellImage FROM spells as s, champions as c WHERE c.id = ? AND c.id = s.id_champions ORDER BY spellButton");
$result->execute(array($id_champions));

$data = $result->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($data);